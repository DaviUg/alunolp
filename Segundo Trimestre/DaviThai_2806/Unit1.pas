unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.Net.URLClient,
  System.Net.HttpClient, System.Net.HttpClientComponent, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    NetHTTPClient1: TNetHTTPClient;
    EdSenha: TEdit;
    EdUser: TEdit;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses Unit2;

procedure TForm1.Button1Click(Sender: TObject);
var cont:String;
begin
  cont := NetHTTPClient1.Get('https://venson.net.br/ws?user='+EdUser.Text+'&pass='+EdSenha.Text).ContentAsString;
  if cont = 'true' then
  begin
    Form2.Label2.Visible := true;
    Form2.Image2.Visible := true;
    Form2.ShowModal();
    Form2.Label2.Visible := false;
    Form2.Image2.Visible := false;
  end
  else
  begin
    Form2.Label1.Visible := true;
    Form2.Image1.Visible := true;
    Form2.ShowModal();
    Form2.Label1.Visible := false;
    Form2.Image1.Visible := false;
  end;
  end;

end.

end.
